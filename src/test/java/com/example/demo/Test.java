package com.example.demo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.wln.Parameter;
import com.example.wln.WanlnConfig;
import com.example.wln.WanlnService;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;

public class Test {

	public void contextLoads() throws UnsupportedEncodingException {

		List<Parameter> parameters = new ArrayList<Parameter>();
		Parameter para = new Parameter("app_key", WanlnConfig.appKey);
		parameters.add(para);
		para = new Parameter("format", "json");
		parameters.add(para);
		String timestamp = String.valueOf(new Date().getTime());
		para = new Parameter("timestamp", timestamp);
		parameters.add(para);

		JSONArray categories = new JSONArray();
		JSONObject categorie = new JSONObject();
		categorie.put("shopNick", WanlnConfig.shopNick);
		categorie.put("categoryID", "101");
		categorie.put("name", "时尚鞋类");
		categorie.put("status", "1");
		categorie.put("sortOrder", "100");
		categories.add(categorie);

		System.out.println(categories.toJSONString());
		para = new Parameter("categories", categories.toString());
		parameters.add(para);

		String sign = "";
		try {
			sign = WanlnService.getToken(parameters, WanlnConfig.appSecret);
			para = new Parameter("sign", sign);
			parameters.add(para);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String paramsJson = JsonKit.toJson(parameters);
		System.out.println("参数: " + paramsJson);

		String url = WanlnConfig.testWanlnUrl + WanlnConfig.version + WanlnConfig.categoryApi + "/?app_key="
				+ WanlnConfig.appKey + "&timestamp=" + timestamp + "&format=json" + "&sign=" + sign + "&categories="
				+ URLEncoder.encode(categories.toString(), "UTF-8");
		System.out.println("请求url: " + url);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		String result = HttpKit.post(url, "", headers);
		System.out.println("返回: " + result);
	}

	@org.junit.Test
	public void addItems() throws UnsupportedEncodingException {

		List<Parameter> parameters = new ArrayList<Parameter>();
		Parameter para = new Parameter("app_key", WanlnConfig.appKey);
		parameters.add(para);
		para = new Parameter("format", "json");
		parameters.add(para);
		String timestamp = String.valueOf(new Date().getTime());
		para = new Parameter("timestamp", timestamp);
		parameters.add(para);

		JSONArray items = new JSONArray();
		JSONObject item = new JSONObject();
		item.put("itemID", "10001");
		item.put("categoryID", "101");
		item.put("shopNick", WanlnConfig.shopNick);
		item.put("title", "测试_匡威高帮经典款");
		item.put("price", 388);
		item.put("quantity", 20);
		item.put("itemURL", "www.shijielianhe.com");
		item.put("imageURL", "www.shijielianhe.com");
		item.put("status", 1);
		item.put("createTime", new Date());
		item.put("modifyTime", new Date());
		items.add(item);

		JSONArray skus = new JSONArray();
		JSONObject sku = new JSONObject();
		sku.put("skuID", "1000101");
		sku.put("itemID", item.get("itemID"));
		sku.put("quantity", 20);
		sku.put("price", 388);
		sku.put("createTime", new Date());
		sku.put("modifiyTime", new Date());
		sku.put("imageURL", "www.shijielianhe.com");
		sku.put("status", 1);
		sku.put("attributes", "颜色:黑白;尺码:41");
		skus.add(sku);
		item.put("skus", skus);

		String itemsjson = "[{\"itemID\":\"1111\",\"categoryID\":\"101\",\"shopNick\":\""+WanlnConfig.shopNick+"\",\"title\":\"匡威经典帆布鞋\",\"itemCode\":\"123\",\"price\":10.0,\"quantity\":10,\"itemURL\":\"http:xxxx/1\",\"imageURL\":\"http://sdf.png\",\"status\":1,\"createTime\":1421854932462,\"modifyTime\":1421854932462,\"properties\":\"factory:china\",\"weight\":0.0,\"skus\":[{\"skuID\":\"111-1\",\"itemID\":\"1111\",\"quantity\":10,\"price\":10.0,\"skuCode\":\"0901\",\"attributes\":\"size:L\",\"createTime\":1421854932462,\"modifiyTime\":1421854932462,\"imageURL\":\"http:cxx.png\",\"status\":1},{\"skuID\":\"111-2\",\"itemID\":\"1111\",\"quantity\":20,\"price\":20.0,\"skuCode\":\"0902\",\"attributes\":\"size:S\",\"createTime\":1421854932462,\"modifiyTime\":1421854932462,\"imageURL\":\"http:cxx.png\",\"status\":1}]}]";

		para = new Parameter("items", items.toJSONString());
		parameters.add(para);

		String sign = "";
		try {
			sign = WanlnService.getToken(parameters, WanlnConfig.appSecret);
			para = new Parameter("sign", sign);
			parameters.add(para);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String paramsJson = JsonKit.toJson(parameters);
		System.out.println("参数: " + paramsJson);

		String url = WanlnConfig.testWanlnUrl + WanlnConfig.version + WanlnConfig.itemApi + "/?app_key="
				+ WanlnConfig.appKey + "&timestamp=" + timestamp + "&format=json" + "&sign=" + sign + "&items="
				+ URLEncoder.encode(items.toJSONString(), "UTF-8");
		System.out.println("请求url: " + url);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		String result = HttpKit.post(url, "", headers);
		System.out.println("返回: " + result);
	}

}
