package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Sku implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long sID;//

	private String skuID;// B2C 系统规格编号

	private String itemID;// B2C 系统商品编号

	private Integer quantity;// 库存

	private Double price;// 单价

	@Column(name = "c_status")
	private Integer status;// 状态: 1：使用中；0：已删除

	private String skuCode;// 商家编码，商家自己输入的编码

	private Long createTime;// 创建时间，毫秒级时间戳，如 1421585369113

	private Long modifiyTime;// 最 新 修 改 时 间 ， 毫 秒 级 时 间 戳 ， 如1421585369113

	private String imageURL;// 规格图片地址

	private Double weight;// 重量

	private String barcode;// 条码

	private String attributes;// 规格：key1:value;key2:value;...以 key:value
								// 的键值形式拼接，必须使用半角符，如颜色:红色;尺码:M

	private static final long serialVersionUID = -8601290514642529913L;

	public Long getsID() {
		return sID;
	}

	public void setsID(Long sID) {
		this.sID = sID;
	}

	public String getSkuID() {
		return skuID;
	}

	public void setSkuID(String skuID) {
		this.skuID = skuID;
	}

	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getModifiyTime() {
		return modifiyTime;
	}

	public void setModifiyTime(Long modifiyTime) {
		this.modifiyTime = modifiyTime;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

}
