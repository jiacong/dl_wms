package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Category implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cID;//

	private String shopNick;// ERP 系统中的店铺昵称

	private String categoryID;// B2C 系统商品类目编号

	private String name;// 类目名称

	private String parentID;// 父类目编号

	@Column(name = "c_status")
	private Integer status;// 状态: 1：使用中；0：已删除

	private Integer sortOrder;// 排序

	public String getShopNick() {
		return shopNick;
	}

	public void setShopNick(String shopNick) {
		this.shopNick = shopNick;
	}

	public Long getcID() {
		return cID;
	}

	public void setcID(Long cID) {
		this.cID = cID;
	}

	public String getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	private static final long serialVersionUID = -8601290514642529913L;

}
