package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 商品信息
 * 
 * @author dr
 *
 */
@Entity
public class Item implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long iID;//

	private String itemID;// 线上系统商品编号

	private String categoryID;// 商品类目编号（属于哪个类目）

	private String shopNick;// ERP 中的店铺昵称
	private String title;// 商品标题
	private String itemCode;// 商家编码，商家自己输入的编码
	private Double price;// 单价

	private Integer quantity;// 库存
	private String itemURL;// 商品地址
	private String imageURL;// 图片地址
	private Integer status;// 状态,0：已删除，1：在售，2：待售，仓库中

	private Long createTime;// 创建时间，毫秒级时间戳，如 1421585369113

	private Long modifyTime;// 最新修改时间，毫秒级时间戳，如 1421585369113

	private String properties;// 商 品 属 性 ， key1:value;key2:value;... 以key:value
								// 的键值形式拼接，必须使用半角符。如生产商:万里牛;原料:实木

	private String brand;// 品牌，如 nike

	private Double weight;// 重量

	private String barcode;// 条码

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Long getiID() {
		return iID;
	}

	public void setiID(Long iID) {
		this.iID = iID;
	}

	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	public String getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	public String getShopNick() {
		return shopNick;
	}

	public void setShopNick(String shopNick) {
		this.shopNick = shopNick;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getItemURL() {
		return itemURL;
	}

	public void setItemURL(String itemURL) {
		this.itemURL = itemURL;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Long modifyTime) {
		this.modifyTime = modifyTime;
	}

	private static final long serialVersionUID = -8601290514642529913L;

}
