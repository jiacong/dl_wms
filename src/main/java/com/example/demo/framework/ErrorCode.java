package com.example.demo.framework;

public enum ErrorCode {

	SUCCESS_10000("10000","SUCCESS"),
	ERROR_10001("10001", "用户名或密码错误"), 
	ERROR_10002("10002", "参数验证不合法"), 
	ERROR_10003("10003", "用户登录已过期，请重新登录"), 
	ERROR_10999("10999", "系统错误");

	private String code;
	private String msg;

	private ErrorCode(String code, String msg) {
		this.setCode(code);
		this.setMsg(msg);
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return this.code + ";" + this.msg;
	}

}
