package com.example.demo.framework.base;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {

	
	@Autowired
	protected HttpServletRequest request;
	
	
	protected String createToken(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	
}
