package com.example.demo.framework.util;

import org.joda.time.DateTime;

public class JodaUtil {

	public static final String DATETIME_FORMAT_01 = "yyyy-MM-dd HH:mm:ss";

	public static String now() {
		return new DateTime().toString(DATETIME_FORMAT_01);
	}
}
