package com.example.demo.framework;

public class BusinessException extends RuntimeException{

	private static final long serialVersionUID = -4051296510023004176L;
	
	
	public BusinessException(Object obj){
		super(obj.toString());
	}

}
