package com.example.demo.framework;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class ResultData implements Serializable {

	private static final long serialVersionUID = 5072538934568729452L;

	private String code;

	private String msg;

	private Object data;

	public ResultData(Object data) {
		super();
		this.code = ErrorCode.SUCCESS_10000.getCode();
		this.msg = ErrorCode.SUCCESS_10000.getMsg();
		this.data = data;
	}

	public ResultData(ErrorCode errorCode, Object data) {
		super();
		this.code = errorCode.getCode();
		this.msg = errorCode.getMsg();
		this.data = data;
	}

	public static void main(String[] args) {
		ResultData data = new ResultData(ErrorCode.ERROR_10001);
		JSONObject json = (JSONObject) JSON.toJSON(data);
		json.put("data", "hasodfhasdflk");
		System.out.println(json.toJSONString());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
