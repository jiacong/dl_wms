package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.framework.BusinessException;
import com.example.demo.framework.ErrorCode;
import com.example.demo.framework.ResultData;
import com.example.demo.framework.base.BaseController;
import com.example.demo.framework.util.JodaUtil;
import com.example.demo.model.UserInfo;
import com.example.demo.repository.UserRepository;

@RestController
public class CommonController extends BaseController {

	Logger log = LoggerFactory.getLogger(CommonController.class);

	@Autowired
	private UserRepository userRepository;

	@RequestMapping("login")
	public Object login(String username, String password) {
		Map<String, Object> data = new HashMap<>();
		UserInfo user = userRepository.getByUsername(username);
		if (user == null || !password.equals(user.getPassword())) {
			throw new BusinessException(ErrorCode.ERROR_10001);
		}
		user.setToken(createToken());
		user.setTokenTime(JodaUtil.now());
		data.put("user", user);
		return new ResultData(data);
	}
	

}
