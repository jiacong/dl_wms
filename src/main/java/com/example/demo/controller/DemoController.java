package com.example.demo.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.framework.base.BaseController;
import com.example.demo.framework.util.MD5;
import com.example.wln.Parameter;
import com.example.wln.WanlnConfig;
import com.example.wln.WanlnService;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;

@RestController
@RequestMapping("demo")
public class DemoController extends BaseController{

	Logger log = LoggerFactory.getLogger(DemoController.class);


	@RequestMapping("wlnTest")
	public Object wlnTest() throws UnsupportedEncodingException {
		
		String timestamp = String.valueOf(new Date().getTime());
		List<Parameter> parameters = new ArrayList<Parameter>();
		Parameter para = new Parameter("app_key", WanlnConfig.appKey);
		parameters.add(para);
		para = new Parameter("format", "json");
		parameters.add(para);
		para = new Parameter("timestamp", timestamp);
		parameters.add(para);
		
		JSONArray itemsArray = new JSONArray();

		JSONObject json = new JSONObject();
		json.put("itemID", "10001");
		json.put("shopNick", WanlnConfig.shopNick);
		json.put("title", "测试_匡威2018最新款帆布鞋");
		json.put("price", "398");
		json.put("quantity", "20");
		json.put("itemURL", "http://www.shijielianhe.cn/goods-info.shtml?cid=1959");
		json.put("imageURL", "http://p1k3sl4k0.bkt.clouddn.com/kjds/test/commodity/0b00f3c08da24512a07f6b994c33e1fc");
		json.put("status", "2");
		json.put("createTime", timestamp);
		json.put("modifyTime", timestamp);

		JSONArray skusArray = new JSONArray();
		JSONObject skus = new JSONObject();
		skus.put("skuID", "1000101");
		skus.put("itemID", "10001");
		skus.put("quantity", "20");
		skus.put("price", "398");
		skus.put("createTime", timestamp);
		skus.put("modifiyTime", timestamp);
		skus.put("imageURL", json.get("imageURL"));
		skus.put("status", json.get("status"));
		skus.put("attributes", "颜色:黑白;尺码:41");
		skusArray.add(skus);
		json.put("skus", skusArray);
		itemsArray.add(json);

		para = new Parameter("items", itemsArray.toJSONString());
		parameters.add(para);

		String sign = "";
		try {
			sign = WanlnService.getToken(parameters, WanlnConfig.appSecret);
			para = new Parameter("sign", sign);
			parameters.add(para);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String paramsJson = JsonKit.toJson(parameters);
		System.out.println("参数: " + paramsJson);

		String url = WanlnConfig.testWanlnUrl + WanlnConfig.version + WanlnConfig.itemApi + "/?app_key="
				+ WanlnConfig.appKey + "&timestamp=" + timestamp + "&format=json" + "&sign=" + sign + "&items="
				+ URLEncoder.encode(json.toString(), "UTF-8");
		System.out.println("请求url: " + url);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		String result = HttpKit.post(url, "", headers);
		System.out.println("返回: " + result);
		return result;
	}

	@Test
	public void test() throws UnsupportedEncodingException {
		
		String timestamp = String.valueOf(new Date().getTime());
		List<Parameter> parameters = new ArrayList<Parameter>();
		Parameter para = new Parameter("app_key", WanlnConfig.appKey);
		parameters.add(para);
		para = new Parameter("format", "json");
		parameters.add(para);
		para = new Parameter("timestamp", timestamp);
		parameters.add(para);

		JSONArray itemsArray = new JSONArray();

		JSONObject json = new JSONObject();
		json.put("itemID", "10001");
		json.put("shopNick", WanlnConfig.shopNick);
		json.put("title", "测试_匡威2018最新款帆布鞋");
		json.put("price", "398");
		json.put("quantity", "20");
		json.put("itemURL", "http://www.shijielianhe.cn/goods-info.shtml?cid=1959");
		json.put("imageURL", "http://p1k3sl4k0.bkt.clouddn.com/kjds/test/commodity/0b00f3c08da24512a07f6b994c33e1fc");
		json.put("status", "2");
		json.put("createTime", timestamp);
		json.put("modifyTime", timestamp);

		JSONArray skusArray = new JSONArray();
		JSONObject skus = new JSONObject();
		skus.put("skuID", "1000101");
		skus.put("itemID", "10001");
		skus.put("quantity", "20");
		skus.put("price", "398");
		skus.put("createTime", timestamp);
		skus.put("modifiyTime", timestamp);
		skus.put("imageURL", json.get("imageURL"));
		skus.put("status", json.get("status"));
		skus.put("attributes", "颜色:黑白;尺码:41");
		skusArray.add(skus);
		json.put("skus", skusArray);
		itemsArray.add(json);

		para = new Parameter("items", itemsArray.toJSONString());
		parameters.add(para);

		String sign = "";
		try {
			sign = WanlnService.getToken(parameters, WanlnConfig.appSecret);
			para = new Parameter("sign", sign);
			parameters.add(para);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String paramsJson = JsonKit.toJson(parameters);
		System.out.println("参数: " + paramsJson);

		String url = WanlnConfig.testWanlnUrl + WanlnConfig.version + WanlnConfig.itemApi + "/?app_key="
				+ WanlnConfig.appKey + "&timestamp=" + timestamp + "&format=json" + "&sign=" + sign + "&items="
				+ URLEncoder.encode(json.toString(), "UTF-8");
		System.out.println("请求url: " + url);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		String result = HttpKit.post(url, "", headers);
		System.out.println("返回: " + result);
	}

	@SuppressWarnings("rawtypes")
	private String createSign(SortedMap<String, Object> packageParams) {
		String app_secret = WanlnConfig.appSecret;
		StringBuffer sb = new StringBuffer(app_secret);
		Set es = packageParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			Object k = entry.getKey();
			Object v = entry.getValue();
			if (null != v && !"".equals(v) && !"sign".equals(k) && !"key".equals(k)) {
				sb.append(k + "" + v);
			}
		}
		sb.append(app_secret);
		System.out.println(sb.toString());
		return MD5.encrytMD5(sb.toString()).toUpperCase();
	}

}
