package com.example.wln;

public final class WanlnConfig {

	// 测试网关
	public static final String testWanlnUrl = "http://114.67.231.99/open/api/";
	public static final String wanlnUrl = "http://open.hupun.com/api/";
	public static final String version = "v1";
	public static final String shopNick = "联韵电商";
	public static final String appKey = "18LY0316";
	public static final String appSecret = "AA610FAA3E4439CAB4A25E24AB3A14D7";

	/** 商品类目推送接口 **/
	public static final String categoryApi = "/categories/open";
	/** 商品推送接口 **/
	public static final String itemApi = "/items/open";
	/** 订单推送接口 **/
	public static final String tradesApi = "/trades/open";

	public static final String inventoriesErpSingleApi = "/inventories/erp/single";

}
